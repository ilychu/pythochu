#in shell run pip install Pillow
# эта функция строит сетку на картинке
# на выходе картинка поделенная на секции

from PIL import Image, ImageDraw
from selenium import webdriver
import os
import sys


class Grid:

    def __init__(self):
        self.capture_image()

    def capture_image(self):
        screenshot = Image.open("screenshots/aaa.png")
        columns = 60
        rows = 80
        screen_width, screen_height = screenshot.size

        block_width = ((screen_width - 1) // columns) + 1 # this is just a division ceiling
        block_height = ((screen_height - 1) // rows) + 1

        for y in range(0, screen_height, block_height):
            for x in range(0, screen_width, block_width):
                draw = ImageDraw.Draw(screenshot)
                draw.rectangle((x, y, x+block_width, y+block_height), outline = "red")

        screenshot.save("screenshots/grid.png")

#вычисление средней яркости
    def process_region(self, image, x, y, width, height):
        region_total = 0

        # This is the sensitivity factor, the larger it is the less sensitive the comparison
        factor = 10

        for coordinateY in range(y, y+height):
            for coordinateX in range(x, x+width):
                try:
                    pixel = image.getpixel((coordinateX, coordinateY))
                    region_total += sum(pixel)/4
                except:
                    return

        return region_total/factor

    #анализ результатов, возвращает число квадратов по которым можно ассёртить
    def analyze(self, screenshot_staging = "screenshots/xxx.png", screenshot_production = "screenshots/yyy.png"):
        screenshot_staging = Image.open(screenshot_staging)
        screenshot_production = Image.open(screenshot_production)
        columns = 60
        rows = 80
        screen_width, screen_height = screenshot_staging.size

        block_width = ((screen_width - 1) // columns) + 1 # this is just a division ceiling
        block_height = ((screen_height - 1) // rows) + 1

        sum_of_diffs= 0
        for y in range(0, screen_height, block_height+1):
            for x in range(0, screen_width, block_width+1):
                region_staging = self.process_region(screenshot_staging, x, y, block_width, block_height)
                region_production = self.process_region(screenshot_production, x, y, block_width, block_height)

                if region_staging is not None and region_production is not None and region_production != region_staging:
                    draw = ImageDraw.Draw(screenshot_staging)
                    draw.rectangle((x, y, x+block_width, y+block_height), outline = "red")
                    sum_of_diffs= sum_of_diffs +1

        screenshot_staging.save("screenshots/result.png")
        return sum_of_diffs


Grid()
