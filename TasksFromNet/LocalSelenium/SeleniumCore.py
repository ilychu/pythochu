from telnetlib import EC
from selenium import webdriver
import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# PyTest, Allure, Requests
class BroDriver:
    chrome_options = webdriver.ChromeOptions()
    chrome_options.set_capability("browserVersion", "67")
    chrome_options.set_capability("platformName", "Windows XP")
    chrome_options.add_argument('whitelisted-ips')
    chrome_options.add_argument('headless')
    chrome_options.add_argument('no-sandbox')
    chrome_options.add_argument('window-size=1200x800')

    STAGING_URL = 'http://www.yahoo.com'
    PRODUCTION_URL = 'http://www.yahoo.com'

    def __init__(self, serverurl="http://localhost:4444/wd/hub"):
# сюда передаётся url сервера с селениумом, в моём случае докер контейнер
        self.serverurl= serverurl
# для дальнейших манипуляций
        self.driver = webdriver.Remote(
              command_executor=serverurl,
                options=webdriver.ChromeOptions())

# чтобы удобно писать GETы
    def get(self, uri="ERROR_NO_URL_IN_PARAMS"):
        if uri=="ERROR_NO_URL_IN_PARAMS":
            print("+++++Ошибка. В GET не переданы параметры URL+++++")
        if ("http://" in uri) or ("http://" in uri):
            self.driver.get(uri)
        else:
            self.driver.get("http://" + uri)

    def click_xpath(self, locator_string):
        self.driver.find_element(By.XPATH, locator_string).click()

    def screenshot(self, file_name):
        self.driver.set_window_size(1024, 768)
        self.driver.save_screenshot(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'screenshots', file_name))
        # другой способ сохранить в текущую директорию self.driver.get_screenshot_as_file('./google.png')
        print("file saved in " + os.path.join(os.path.dirname(os.path.realpath(__file__))) + "\screenshots")
        self.driver.get_screenshot_as_png()
        print("Save complete")




#     не доделал т.к. не нашел применения, на проекте сделать
#     def waitImplict (self):
#         try:
#     element = WebDriverWait(driver, 10).until(
#         EC.presence_of_element_located((By.ID, "myDynamicElement"))
#     )
# finally:
# driver.quit()


# выгрузить драйвер
    def destroy(self):
        self.driver.quit()



