# пример инициализации selenium драйвера в докер контейнере
# на всякий случай два способа идентичны друг другу, только desired_capabilities устаревший
import time
from telnetlib import EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# устаревший способ инициализации, просто для понимания что такое есть тут в DesiredCapabilities передавались опции как в json
# driver = webdriver.Remote("http://127.0.0.1:4444/wd/hub", DesiredCapabilities.CHROME)

# актуальный способ инициализации
# настройки передаваемые в драйвер
# пока просто пример, потом доделать вынеся в отдельный класс
from selenium.webdriver.support.wait import WebDriverWait

chrome_options = webdriver.ChromeOptions()
chrome_options.set_capability("browserVersion", "67")
chrome_options.set_capability("platformName", "Windows XP")
chrome_options.add_argument('whitelisted-ips')
chrome_options.add_argument('headless')
chrome_options.add_argument('no-sandbox')
chrome_options.add_argument('window-size=1200x800')

driver = webdriver.Remote(
    command_executor='http://localhost:4444/wd/hub',
    options=webdriver.ChromeOptions()
)
# просто вызов
# driver.get('http://www.google.com/')
# time.sleep(5)

driver.get("http://rbc.ru")
# явное ожидание
try:
    element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "myDynamicElement"))
    )
finally:
    driver.quit()




driver.quit()