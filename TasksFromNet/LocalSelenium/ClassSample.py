# class SeleniumTester:
#     pass
#
# object_of_my_class = SeleniumTester()
# print(type(object_of_my_class))

class Car:
    wheels_number = 4
    def __init__(self, name, color, year, is_crashed):
        self.name = name
        self.color = color
        self.year = year
        self.is_crashed =is_crashed


mazda_car = Car(name = 'Mazda cx7', color = 'red', year= 2017, is_crashed = True)

print(mazda_car.name)

bmw_car = Car (name = 'Bmw', color = 'black', year= 2018, is_crashed = False)

print(bmw_car.name)
print(bmw_car.color)
print(bmw_car.year)
print(bmw_car.is_crashed)
print(bmw_car.wheels_number)


number_of_whels_of_three_cars = Car.wheels_number *3
print(number_of_whels_of_three_cars)