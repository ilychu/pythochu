class Car:
    wheels_number = 4
    def __init__(self, name, color, year, is_crashed):
        self.name = name
        self.color = color
        self.year = year
        self.is_crashed =is_crashed

    def drive(self):
        print(self.name + 'Car drives')

opel_car = Car('Opel Tigra', "grey", 1999, True)
opel_car.drive()
print(opel_car.name)
print(opel_car.color)
print(opel_car.year)
print(opel_car.is_crashed)