# сделать конвертер числел в формат exel

import json

def to_excel_format(number: int) -> str:
    string = ""
    while number > 0:
         number, remainder = divmod(number - 1, 26)
         string = chr(65 + remainder) + string
    return string

if __name__ == "__main__":
    input_str = input()
    number = int(input_str)
    answer = to_excel_format(number)
    print(json.dumps(answer))