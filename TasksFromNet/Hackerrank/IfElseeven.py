#!/bin/python3
# If  is odd, print Weird
# If  is even and in the inclusive range of 2 to 5, print Not Weird
# If  is even and in the inclusive range of  6 to 20 , print Weird
# If  is even and greater than 20, print Not Weird

import math
import os
import random
import re
import sys

def res(n):
    if (n%2==0 and n>20):
        return "Not Weird"
    elif (n%2==0 and n>=2 and n<=5 ):
        return "Not Weird"
    else:
        return "Weird"


if __name__ == '__main__':
    n = int(input().strip())
    print(res(n))


# а можно решить так
# n = int(input().strip())
# check = {True: "Not Weird", False: "Weird"}
#
# print(check[
#           n%2==0 and (
#                   n in range(2,6) or
#                   n > 20)
#           ])