# d = {'alpha': [20, 30, 40]}
# print(sum(d['alpha'])/len(d['alpha']))
# вывести среднеее по запросу из словаря переданного на инпут с точностью 2 знака после запятой

if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()
    print(format(sum(student_marks[query_name])/len(student_marks[query_name]), ".2f"))

    # вариант форматирования print("{0:.2f}".format(sum(query_scores)/(len(query_scores))))