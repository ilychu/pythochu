# создать список массивов без повторений в последовательности x y z в сумме не более N

# x=1
# y=1
# z=1
# n=2
# ввод 4 раза то же что и закоменчено выше
x, y, z, n = (int(input()) for _ in range(4))

xlist = [x for x in range(x+1)]
ylist = [y for y in range(y+1)]
zlist = [z for z in range(z+1)]

print(list([xx,yy,zz] for xx in xlist for yy in ylist for zz in zlist if xx+yy+zz!=n))
# если в выводе не нужен лист
# print(*([eachX, eachY , eachZ] for eachX in [x for x in range(x)] for eachY in [y for y in range(y)] for eachZ in [z for z in range(z)] if eachX+eachY+eachZ!=n))
