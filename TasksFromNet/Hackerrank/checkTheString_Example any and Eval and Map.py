# проверить из чего состоит строка на входе
# если хоть один символ подходит под выражения, то вернуть тру
# сами выражения в комментах проверяют строку целиком а не отдельный символ, в этом сложность
# def lol(s):
#       #что проверяем: как проверки ниже но только каждый элемент
#       # print( str.isalnum(s)==True)
#       # print( str.isalpha(s)==True)
#       # print( str.isdigit(s)==True)
#       # # print( str.islower(s)==True)
#       # # print( str.isupper(s)==True)
#       for test in ('isalnum', 'isalpha', 'isdigit', 'islower', 'isupper'):
#           print(  any(eval("elem." + test + "()") for elem in s))


if __name__ == '__main__':
    s = input()
    for test in ('isalnum', 'isalpha', 'isdigit', 'islower', 'isupper'):
        print(  any(eval("elem." + test + "()") for elem in s))


# Технически возможно более простые вещи:
# for method in [str.isalnum, str.isalpha, str.isdigit, str.islower, str.isupper]:
#     print any(method(c) for c in s)
# or
#
# t = type(s)
# for method in [t.isalnum, t.isalpha, t.isdigit, t.islower, t.isupper]:
#     print any(method(c) for c in s)
#
# или вариант с map
#
# s=raw_input()
# print (any(map(str.isalnum,s)))
# print (any(map(str.isalpha,s)))
# print (any(map(str.isdigit,s)))
# print (any(map(str.islower,s)))
# print (any(map(str.isupper,s)))