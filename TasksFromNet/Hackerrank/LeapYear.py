# Год можно без остатка разделить на 4, это високосный год, если:
# Год можно разделить поровну на 100, это НЕ високосный год, если:
# Год также делится на 400 без остатка. Тогда это високосный год.

def checker (year):
   if (year%4==0) and (year%100!=0):
       return True
   else:
       if (year%400==0):
             return True
       else:
           return False

print(checker(1992))

# однострочник:
# v1
# return year % 4 == 0 and (year % 400 == 0 or year % 100 != 0)
# v2
# if year%4 == 0 and (year%100 != 0 or year%400 == 0): leap = True

